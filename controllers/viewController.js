const path = require('path')

// LOGIN_PAGE
exports.getLoginForm = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','login.html'))
}

// Sign up page
exports.getSignupForm = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','signup.html'))
}


// homepage
exports.getHome = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','dashboard.html'))
}

// profile
exports.getProfile = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','myprofilepage.html'))
}
